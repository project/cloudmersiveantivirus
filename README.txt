Cloudmersive Anti-virus Scanner

This module integrates with the anti-virus scanner Cloudmersive Antivirus API.

Uploaded files are forwarded to the Cloudmersive Antivirus service and scanned.

Infected files are blocked in the validation routine, so they cannot be saved.

Administrator's can configure scanning preferences via the administration configuration UI.